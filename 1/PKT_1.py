#https://onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=278&page=show_problem&problem=3829

class RecreateField:
    def __init__(self):
        self.solutionCount = 1
        self.position = ('a','b','c','d','e','f','g','h','i')
    
    def FindTheStartingField(self, commands):
        Matrix = [[0 for x in range(3)] for y in range(3)]
        command = list(commands)
        
        for c in commands:
            Matrix = self.__SolveByCommand(Matrix, c)
        
        rez = open ("rez.txt", "a")
        rez.write(self.__printField(Matrix))
        rez.close()
    
    def __SolveByCommand(self, field, command):
        if command == self.position[0]:
            field = self.__AddByPosition(field, 0, 0)
        elif command == self.position[1]:
            field = self.__AddByPosition(field, 0, 1)
        elif command == self.position[2]:
            field = self.__AddByPosition(field, 0, 2)
        elif command == self.position[3]:
            field = self.__AddByPosition(field, 1, 0)
        elif command == self.position[4]:
            field = self.__AddByPosition(field, 1, 1)
        elif command == self.position[5]:
            field = self.__AddByPosition(field, 1, 2)
        elif command == self.position[6]:
            field = self.__AddByPosition(field, 2, 0)
        elif command == self.position[7]:
            field = self.__AddByPosition(field, 2, 1)
        elif command == self.position[8]:
            field = self.__AddByPosition(field, 2, 2)
        return field
    
    def __AddByPosition(self, field, pos1, pos2):
        field[pos1][pos2] +=1
        if field[pos1][pos2] > 9:
            field[pos1][pos2] = 0
        if pos1 == 0:
            field[pos1 + 1][pos2] +=1
            if field[pos1 + 1][pos2] > 9:
                field[pos1 + 1][pos2] = 0
        if pos1 == 1:
            field[pos1 + 1][pos2] +=1
            if field[pos1 + 1][pos2] > 9:
                field[pos1 + 1][pos2] = 0
            field[pos1 - 1][pos2] +=1
            if field[pos1 - 1][pos2] > 9:
                field[pos1 - 1][pos2] = 0
        if pos1 == 2:
            field[pos1 - 1][pos2] +=1
            if field[pos1 - 1][pos2] > 9:
                field[pos1 - 1][pos2] = 0
        
        if pos2 == 0:
            field[pos1][pos2 + 1] +=1
            if field[pos1][pos2 + 1] > 9:
                field[pos1][pos2 + 1] = 0
        if pos2 == 1:
            field[pos1][pos2 + 1] +=1
            if field[pos1][pos2 + 1] > 9:
                field[pos1][pos2 + 1] = 0
            field[pos1][pos2 - 1] +=1
            if field[pos1][pos2 - 1] > 9:
                field[pos1][pos2 - 1] = 0
        if pos2 == 2:
            field[pos1][pos2 - 1] +=1
            if field[pos1][pos2 - 1] > 9:
                field[pos1][pos2 - 1] = 0
        return field

    def __printField(self, field):
        solution = "Case #{0}: \n".format(self.solutionCount)
        self.solutionCount += 1
        for i in field:
            solution += "{0} {1} {2} \n".format(i[0], i[1], i[2])
        solution += "\n"
        return solution

x = RecreateField()

r = open ("rez.txt", "w")
r.close()

duom = open("duom.txt", "r")
lines = duom.readlines()
duom.close()

for i in lines:
    i = i.replace ("\n", "")
    x.FindTheStartingField(i)